#ifndef ROGUELIKE_H
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#define DLIST_INIT(x) \
    (x)->Next = (x); \
    (x)->Prev = (x)

#define DLIST_ADD(x, y) \
    (y)->Next = (x)->Next; \
    (y)->Prev = (x); \
    (x)->Next = (y); \
    (y)->Next->Prev = (y)

#define DLIST_REMOVE(x) \
    (x)->Next->Prev = (x)->Prev; \
    (x)->Prev->Next = (x)->Next

typedef struct message_t
{
    struct message_t *Next;
    struct message_t *Prev;
    char *Message;
} message;
// TODO(marshel): Consider replacing this with a stream FIFO
typedef struct circle_buffer_t
{
    message Messages;
    message *Sentinal;
    i32 MaxCount;
    i32 Count;
} circle_buffer;

enum
{
    TILE_TYPE_NONE,
    TILE_TYPE_FLOOR,
    TILE_TYPE_WALL,
    TILE_TYPE_DOOR_CLOSED,
    TILE_TYPE_DOOR_OPEN,
    TILE_TYPE_STAIRS_DOWN,
    TILE_TYPE_STAIRS_UP,
    TILE_TYPE_SCROLL,
    TILE_TYPE_POTION,

    TILE_TYPE_COUNT,
};
char TileDisplay[TILE_TYPE_COUNT] =
{
    ' ', // TILE_TYPE_NONE
    '.', // TILE_TYPE_FLOOR
    '#', // TILE_TYPE_WALL
    '+', // TILE_TYPE_DOOR_CLOSED
    '/', // TILE_TYPE_DOOR_OPEN
    '<', // TILE_TYPE_STAIRS_DOWN
    '>', // TILE_TYPE_STAIRS_UP
    '?', // TILE_TYPE_SCROLL
    '!', // TILE_TYPE_POTION
};

typedef struct tile_t
{
    i32 Type;
} tile;
typedef struct map_t
{
    // TODO(marshel): DLIST for dungeon levels?
    i32 Width;
    i32 Height;
    tile *Tiles;
} map;

typedef struct v2_t
{
    union
    {
        i32 X;
        i32 H;
    };

    union
    {
        i32 Y;
        i32 W;
    };
} v2;


typedef struct camera_t
{
    v2 Pos;
    v2 Dimensions;
} camera;

typedef struct game_state_t
{
    CursesWindow *MessageWindow;
    CursesWindow *MapWindow;
    CursesWindow *PlayerStatsWindow;

    map DungeonMap;
    circle_buffer Messages;
    camera Camera;

    i32 WindowMaxX;
    i32 WindowMaxY;

    i32 MapWindowWidth;
    i32 MapWindowHeight;

    v2 PlayerPos;

    b32 Running;
} game_state;

i32 StringLength(char *String)
{
    i32 Result = 0;

    while (*String)
    {
        ++String;
        ++Result;
    }

    return Result;
}

void StringCopy(char *Source, char *Destination)
{
    while (*Source)
    {
        *Destination++ = *Source++;
    }
    *Destination = 0;
}

tile * MapGetTile(map *Map, i32 X, i32 Y)
{
    tile *Result = 0;
    i32 MapSize = Map->Width * Map->Height;
    i32 MapX = X + (Map->Width / 2);
    i32 MapY = Y + (Map->Height / 2);
    i32 TilePosition = (Map->Width * MapY) + MapX;

    if (TilePosition > MapSize)
    {
        Result = 0;
    } else {
        Result = &Map->Tiles[TilePosition];
    }

    return Result;
}

void AppendMessage(circle_buffer *Messages, char *Msg)
{
    message *NewMessage;
    if (Messages->Count == Messages->MaxCount)
    {
        message *OldMessage = Messages->Messages.Prev;
        if (OldMessage != Messages->Sentinal)
        {
            DLIST_REMOVE(OldMessage);
            if (OldMessage->Message)
            {
                free(OldMessage->Message);
            }
            free(OldMessage);
            --Messages->Count;
        }
    }

    NewMessage          = malloc(sizeof(*NewMessage));
    NewMessage->Message = malloc(StringLength(Msg) + 1);
    DLIST_INIT(NewMessage);
    StringCopy(Msg, NewMessage->Message);
    DLIST_ADD(&Messages->Messages, NewMessage);
    ++Messages->Count;
}

v2 WorldFromScreenCoordinates(game_state *GameState, i32 ScreenX, i32 ScreenY)
{
    v2 Result = {0};
    v2 CameraP = GameState->Camera.Pos;
    i32 CameraWidth = GameState->Camera.Dimensions.W;
    i32 CameraHeight = GameState->Camera.Dimensions.H;

    Result.X = (CameraP.X - (CameraWidth / 2)) + ScreenX;
    Result.Y = (-CameraP.Y - (CameraHeight / 2)) + ScreenY;

    return Result;
}

v2 ScreenFromWorldCoordinates(game_state *GameState, i32 WorldX, i32 WorldY)
{
    v2 Result = {0};
    v2 CameraP = GameState->Camera.Pos;
    i32 CameraWidth = GameState->Camera.Dimensions.W;
    i32 CameraHeight = GameState->Camera.Dimensions.H;

    Result.X = -(CameraP.X - (CameraWidth / 2)) + WorldX;
    Result.Y = CameraP.Y + (CameraHeight / 2) - WorldY;

    return Result;
}

#define ROGUELIKE_H
#endif
