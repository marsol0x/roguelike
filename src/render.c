/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

void RenderMessages(game_state *GameState)
{
    i32 i;
    message *Message;

    werase(GameState->MessageWindow);
    box(GameState->MessageWindow, '|', '-');

    i = 3;
    Message = GameState->Messages.Messages.Next;
    while (i && (Message != GameState->Messages.Sentinal))
    {
        mvwprintw(GameState->MessageWindow, i, 1, Message->Message);
        Message = Message->Next;
        --i;
    }

    wrefresh(GameState->MessageWindow);
}

void RenderPlayerStats(game_state *GameState)
{
    werase(GameState->PlayerStatsWindow);
    box(GameState->PlayerStatsWindow, '|', '-');
    wrefresh(GameState->PlayerStatsWindow);
}

void RenderMap(game_state *GameState)
{
    i32 ScreenX, ScreenY;
    v2 ScreenPos;
    v2 WorldPos;
    i32 CameraHeight = GameState->Camera.Dimensions.H;
    i32 CameraWidth = GameState->Camera.Dimensions.W;

    werase(GameState->MapWindow);

    for (ScreenY = 0; ScreenY < CameraHeight; ++ScreenY)
    {
        for (ScreenX = 0; ScreenX < CameraWidth; ++ScreenX)
        {
            tile *Tile;
            char DisplayTile = TileDisplay[TILE_TYPE_NONE];

            WorldPos = WorldFromScreenCoordinates(GameState, ScreenX, ScreenY);
            Tile = MapGetTile(&GameState->DungeonMap, WorldPos.X, WorldPos.Y);
            if (Tile)
            {
                DisplayTile = TileDisplay[Tile->Type];
            }

            mvwaddch(GameState->MapWindow, ScreenY, ScreenX, DisplayTile);
        }
    }

    ScreenPos = ScreenFromWorldCoordinates(GameState, GameState->PlayerPos.X, GameState->PlayerPos.Y);
    mvwaddch(GameState->MapWindow, ScreenPos.Y, ScreenPos.X, '@');

    wrefresh(GameState->MapWindow);
}

