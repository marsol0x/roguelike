/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

/*
 * TODO:
 *
 * - Memory allocation needs to be better than malloc
 *   - Use a memory arena
 */
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <curses.h>

#if DEBUG
#include <signal.h>
#else
#define raise(x)
#endif

#define CursesWindow WINDOW

#define ArrayCount(x) (sizeof(a)/sizeof((a)[0]))

typedef char                i8;
typedef short              i16;
typedef int                i32;
typedef long long          i64;

typedef int                b32;
#define True (1==1)
#define False (!(True))

typedef unsigned char       u8;
typedef unsigned short     u16;
typedef unsigned int       u32;
typedef unsigned long long u64;

typedef float              r32;
typedef double             r64;

#include "roguelike.h"
#include "render.c"

#define TEST_MESSAGE_COUNT 7
i32 TestMessageIndex;
char *testMessages[TEST_MESSAGE_COUNT] = {
    "(Debug) A south witches the foam before the ambient cylinder.",
    "My breakdown caps every race next to the perfect cry.",
    "Past a warm synthesis lies the fleet optic.",
    "(Debug) The algorithm tires the void beard.",
    "Her contract listens across the bitter sore.",
    "(Debug) The overseas door calculates.",
    "(Debug) Over the ritual exponential chooses the preceding pedant.",
};

b32 DoesCollide(game_state *GameState, u32 X, u32 Y)
{
    b32 Result = False;
    tile *Dest = MapGetTile(&GameState->DungeonMap, X, Y);

    if (Dest)
    {
        // TODO(marshel): Expand on tiles so that collision is a detectable
        // boolean  
        switch (Dest->Type)
        {
            case TILE_TYPE_WALL:
            {
                Result = True;
            } break;
        }
    }

    return Result;
}

void HandleInput(game_state *GameState)
{
    i32 Input;

    Input = getch();
    switch (Input)
    {
        case 'q':
        {
            GameState->Running = False;
        } break;

        case 'k':
        case KEY_UP:
        {
            if (!DoesCollide(GameState, GameState->PlayerPos.X, GameState->PlayerPos.Y + 1))
            {
                ++GameState->PlayerPos.Y;
            }
        } break;

        case 'j':
        case KEY_DOWN:
        {
            if (!DoesCollide(GameState, GameState->PlayerPos.X, GameState->PlayerPos.Y - 1))
            {
                --GameState->PlayerPos.Y;
            }
        } break;

        case 'h':
        case KEY_LEFT:
        {
            if (!DoesCollide(GameState, GameState->PlayerPos.X - 1, GameState->PlayerPos.Y))
            {
                --GameState->PlayerPos.X;
            }
        } break;

        case 'l':
        case KEY_RIGHT:
        {
            if (!DoesCollide(GameState, GameState->PlayerPos.X + 1, GameState->PlayerPos.Y))
            {
                ++GameState->PlayerPos.X;
            }
        } break;

        case 'm':
        {
            if (TestMessageIndex == TEST_MESSAGE_COUNT)
            {
                TestMessageIndex = 0;
            }
            AppendMessage(&GameState->Messages, testMessages[TestMessageIndex++]);
        } break;

        // NOTE(marshel): When the terminal is resized we get KEY_RESIZE
        case KEY_RESIZE:
        {
            GameState->WindowMaxX        = getmaxx(stdscr);
            GameState->WindowMaxY        = getmaxy(stdscr);
            GameState->MessageWindow     = subwin(stdscr, 5, GameState->WindowMaxX, 0, 0);
            GameState->PlayerStatsWindow = subwin(stdscr, 12, GameState->WindowMaxX, GameState->WindowMaxY - 12, 0);
            GameState->MapWindow         = subwin(stdscr, GameState->WindowMaxY - 17, GameState->WindowMaxX, 5, 0);
            GameState->MapWindowWidth    = getmaxx(GameState->MapWindow);
            GameState->MapWindowHeight   = getmaxy(GameState->MapWindow);

            GameState->Camera.Dimensions.W = GameState->MapWindowWidth;
            GameState->Camera.Dimensions.H = GameState->MapWindowHeight;
        } break;
    }
}

i32 main(i32 argc, char **argv)
{
    game_state GameState = {0};
    size_t TileSize;

    raise(SIGSTOP);

    // NOTE(marshel): Initialize curses
    initscr();
    noecho();
    cbreak();
    curs_set(0);
    nodelay(stdscr, True);
    keypad(stdscr, True);

    // NOTE(marshel): Game goes here
    GameState.WindowMaxX        = getmaxx(stdscr);
    GameState.WindowMaxY        = getmaxy(stdscr);
    GameState.MessageWindow     = subwin(stdscr, 5, GameState.WindowMaxX, 0, 0);
    GameState.PlayerStatsWindow = subwin(stdscr, 12, GameState.WindowMaxX, GameState.WindowMaxY - 12, 0);
    GameState.MapWindow         = subwin(stdscr, GameState.WindowMaxY - 17, GameState.WindowMaxX, 5, 0);
    GameState.MapWindowWidth    = getmaxx(GameState.MapWindow);
    GameState.MapWindowHeight   = getmaxy(GameState.MapWindow);

    // NOTE(marshel): Center the player
    GameState.PlayerPos.X = 0;
    GameState.PlayerPos.Y = 0;

    // NOTE(marshel): Camera
    GameState.Camera.Pos.X = GameState.PlayerPos.X;
    GameState.Camera.Pos.Y = GameState.PlayerPos.Y;
    GameState.Camera.Dimensions.W = GameState.MapWindowWidth;
    GameState.Camera.Dimensions.H = GameState.MapWindowHeight;

    // NOTE(marshel): Messages
    DLIST_INIT(&GameState.Messages.Messages);
    GameState.Messages.Sentinal = &GameState.Messages.Messages;
    GameState.Messages.MaxCount = 4;

    // NOTE(marshel): Maps
    GameState.DungeonMap.Width  = 1024; //GameState.MapWindowWidth;
    GameState.DungeonMap.Height = 1024; //GameState.MapWindowHeight;

    TileSize = sizeof(tile) * GameState.DungeonMap.Width * GameState.DungeonMap.Height;
    GameState.DungeonMap.Tiles = malloc(TileSize);
    memset(GameState.DungeonMap.Tiles, 0, TileSize);

    // NOTE(marshel): Test map
    {
        i32 TopLeftX, TopLeftY, BottomRightX, BottomRightY;
        i32 x, y;
        tile *WallTile = 0;

        TopLeftX = -10;
        TopLeftY = 5;
        BottomRightX = 10;
        BottomRightY = -5;

        for (y = TopLeftY; y >= BottomRightY; --y)
        {
            for (x = TopLeftX; x <= BottomRightX; ++x)
            {
                WallTile = MapGetTile(&GameState.DungeonMap, x, y);
                assert(WallTile);
                WallTile->Type = TILE_TYPE_FLOOR;

                if (y == TopLeftY || y == BottomRightY)
                {
                    WallTile->Type = TILE_TYPE_WALL;
                }

                if (x == TopLeftX || x == BottomRightX)
                {
                    WallTile->Type = TILE_TYPE_WALL;
                }
            }
        }
    }

    GameState.Running = True;

    // NOTE(marshel): Main game loop
    while (GameState.Running)
    {
        HandleInput(&GameState);
        GameState.Camera.Pos.X = GameState.PlayerPos.X;
        GameState.Camera.Pos.Y = GameState.PlayerPos.Y;

        RenderMessages(&GameState);
        RenderPlayerStats(&GameState);
        RenderMap(&GameState);
    }

    // NOTE(marshel): Finish curses
    endwin();
    return 0;
}
